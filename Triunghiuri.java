import java.util.*;

//obiect care mapeaza datele de la intrare si verifica sa fie un triunghi oarecare
class Triangle{
    private double x, y, z;
    private int index;
    private double heron;
    
    public Triangle(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    //intorc index-ul la sfarsit
    public int getIndex(){
        return this.index;
    }

    //setez index-ul pentru fiecare set de valori
    public void setIndex(int index){
        this.index = index;
    }
    
    //verific daca ccele 3 valori pot forma un triunghi
    public boolean isTriangle(){
        if(x < y + z && y < x + z && z < x + y){
            return true;
        }else{
            return false;
        }
    }
    
    public boolean isEquilateral(){
        if(x == y && x == z && z == y){
            return true;
        }else{
            return false;
        }
    }
    
    public boolean isIsoscel(){
        if((x == y) && (y != z) || (x == z) && (z !=y) || (z == y) && (y != x)){
            return true;
        }else{
            return false;
        }
    }
    
    public boolean isAngled(){
        if(Math.pow(x,2) == Math.pow(y, 2) + Math.pow(z, 2)
            || Math.pow(y, 2) == Math.pow(x,2) + Math.pow(z, 2) 
            || Math.pow(z, 2) == Math.pow(x,2) + Math.pow(y, 2)){
            return true;
        }else{
            return false;
        }
    }
    
   //calculez semiperimetrul
    public double getSemiperimeter(){
        return (this.x + this.y + this.z) / 2;
    }
    
    //calculez aria
    public double getArea(){
        return Math.sqrt(this.getSemiperimeter() * (this.getSemiperimeter() - x) *
                (this.getSemiperimeter() - y) * (this.getSemiperimeter() - z));
    }
    
    //calculez perimetrul pentru raportul de mai tarziu
    public double getPerimeter(){
        return this.x + this.y + this.z;
    }
    
    //intorc valoarea calculata mai jos in setHeronFormula()
    public double getHeron(){
        return this.heron;
    }
    
    //calculez raportul
    public double setHeronFormula(){
        heron = this.getArea() / this.getPerimeter();
        return heron;
    }

}

public class Vasi{
    public static void main(String [] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        List<Triangle> list = new ArrayList<>();
        
        //citire date de la tastatura
        for(int i = 0; i < n; i++){
            double x = sc.nextDouble();
            double y = sc.nextDouble();
            double z = sc.nextDouble();
            Triangle t = new Triangle(x, y, z);
            t.setIndex(i);
            list.add(t);
        }
        
        //daca este triunghi oarecare ii calculez raportul arie/perimetru cu metoda setHeronFormula()
        for(int i = 0; i < list.size(); i++){
            if(list.get(i).isTriangle() == true && list.get(i).isEquilateral() == false &&
                list.get(i).isAngled() == false && list.get(i).isIsoscel() == false){
                list.get(i).setHeronFormula();
            }
        }
        
        //imi sortez lista dupa valorile raportului si asa voi avea valoarea ce mai mare
        //la sfarsitul listei
        Collections.sort(list ,Comparator.comparing(Triangle :: getHeron));
        
        int index = list.get(list.size() - 1).getIndex();
        double maxReport = list.get(list.size() - 1).getHeron();
        
        //printez index + raportul  cu 3 zecimale
        System.out.print(index + " "+ String.format("%.3f", maxReport));
    }
}