import java.util.*;

public class Vasi{
    public static void main(String [] args){
        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();
        int n = sc.nextInt();
        int p = sc.nextInt();
        
        int [][] v = new int[m][n];
        int [][] w = new int[n][p];
        
        int [] x = new int[m];
        int [] s = new int[n];
        int [] y = new int[p];
        
        for(int i=0; i < m; i++){
            for(int j = 0; j< n; j++){
                v[i][j] = sc.nextInt();
            }
        }
        
        for(int i = 0; i < n; i++){
            for(int j = 0; j < p; j++){
                w[i][j] = sc.nextInt();
            }
        }
        
        for(int i = 0; i < m; i++){
            x[i] = sc.nextInt();
        }
        
        int sumOfFirstMatrix = 0;

        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                sumOfFirstMatrix += v[j][i] * x[j];  
            }
            s[i] = sumOfFirstMatrix;
            sumOfFirstMatrix = 0;
        }
        
        int sumOfSecondMatrix = 0;
        
        for(int i = 0; i < p; i++){
            for(int j = 0; j < n; j++){
                sumOfSecondMatrix += s[j] * w[j][i];
            }
            y[i] = sumOfSecondMatrix;
            sumOfSecondMatrix = 0;
        }
        int max = y[0];
        int indexOfMax = 0;
        for(int i = 0; i < y.length; i++){
            if(y[i] > max){
                max = y[i];
                indexOfMax++;
            }
        }
        System.out.println(indexOfMax + 1);
    }
}