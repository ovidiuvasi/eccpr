import java.util.*;

public class Vasi{
    public static void main(String [] args){
        //citirea datelor din nou un pain in the ass
        //pentru ca nu ai o limita de citire 
        //eu am citit intreaga linie pur si simplu nu stiu cum e la voi
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        sc.nextLine();
        String line = sc.nextLine();
        //fac split dupa spatiu ca sa am datele fix cum le primesc
        String [] splitLine = line.split("\\s+");
        List<Integer> list = new ArrayList<>();
        
        //le pun intr-o lista
        for(String s : splitLine){
            list.add(Integer.valueOf(s.trim()));
        }
        
        int suma = 0;
        
        List<Integer> listHelper = new ArrayList<>();
        //lista asta este cu toate valorile de la 1 la N
        for(int i = 0; i < N; i++){
            listHelper.add(i+1);
        }
        
        //vreau sa raman doar cu valorile de la 1 la N 
        //nu le am pe toate deoarece dimensiunea listei initiale este necunoscuta
        for(int i = 0; i < listHelper.size(); i++){
            if(listHelper.get(i) > N){
                listHelper.remove(listHelper.get(i));
            }
        }
        //in a doua lista am elemente de la 1 .. N 
        //asa ca le voi sterge pe toate care sunt in prima lista
        //adica datele de la intrare si asa raman cu valorile care ma intereseaza
        for(int i = 0; i < list.size(); i++){
            listHelper.remove(list.get(i));
        }
        //fac suma lor
        for(int i = 0; i < listHelper.size(); i++){
            suma +=listHelper.get(i);
        }
        System.out.println(suma);
    }
}