import java.util.*;

class Dates {

    //modelarea obiectului dupa datele de la intrare
    private String ziua;
    private String separator;
    private String luna;
    
    public Dates(String ziua, String separator, String luna){
        this.ziua = ziua;
        this.separator = separator;
        this.luna = luna;
    }
    
    public String getZiua(){
        return this.ziua;
    }
    
    public String getSeparator(){
        return this.separator;
    }
    
    public String getLuna(){
        return this.luna;
    }
    
    @Override
    public String toString(){
        return ziua + separator + luna;
    }
    
    //Aceste doua metode cu @Override sunt pentru a putea ordona dupa ce parametrii imi doresc eu 
    //obiectele adaugate in lista. Cel mai probabil voi nu aveti nevoie de asa ceva
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dates dates = (Dates) o;

        if (ziua != null ? !ziua.equals(dates.ziua) : dates.ziua != null) return false;
        if (separator != null ? !separator.equals(dates.separator) : dates.separator != null) return false;
        return luna != null ? luna.equals(dates.luna) : dates.luna == null;
    }

    @Override
    public int hashCode() {
        int result = ziua != null ? ziua.hashCode() : 0;
        result = 31 * result + (separator != null ? separator.hashCode() : 0);
        result = 31 * result + (luna != null ? luna.hashCode() : 0);
        return result;
    }
    
}

public class Vasi{
    public static void main(String [] args){
        Scanner sc= new Scanner(System.in);
        int nrZileDeNastere = sc.nextInt();
        List<Dates> list = new ArrayList<>();
        //citirea de date un pain in the ass pentru ca linia aia nu poate fi split-uita de la inceput in variabile separate
        //asa ca am citit un string dupa care l-am spart pe rand fiind de fiecare data index-i la fel
        for(int i = 0; i < nrZileDeNastere; i++){
            String line = sc.next();
            String ziua = line.trim().substring(0,2);
            String sep = line.trim().substring(2,3);
            String luna = line.trim().substring(3,5);
            Dates d = new Dates(ziua, sep, luna);
            list.add(d);
        }
        //ordonare crescatoare dupa luna si dupa zi
        Collections.sort(list ,Comparator.comparing(Dates :: getLuna).thenComparing(Dates :: getZiua));
        //adaug listea mea intr-o LinkedHashSet care imi elimina duplicatele
        Set<Dates> set = new LinkedHashSet<>(list);
        //afisare date din set-ul de mai sus
        for(Dates d : set){
            System.out.println(d);
        }
    }
}