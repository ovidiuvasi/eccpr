import java.util.*;

public class Vasi{
    
    public static void main(String [] args){
        Scanner sc = new Scanner(System.in);
        int dimensiuneVector = sc.nextInt();
        double [] array = new double[dimensiuneVector];
        
        for(int i = 0; i < dimensiuneVector; i++){
            array[i] = sc.nextDouble();
        }
        
        int lungimeMax = 0;
        int indexMax = -1;
        int indexPrincipal = 0;
        double sumaMax = 0;
        //pornesc de la premiza ca nu vreau sa mi se depaseasca lungimea vectorului
        //pentru a nu primi o exceptie/eroarea si interez pana la ultimul element
        //inclusiv
        while(indexPrincipal < array.length){
            //sunt interesat doar de numerele pozitive
            if(array[indexPrincipal] > 0){
                //index-ul secundar va intera pe sub vector ca o a doua iterare
                //iar indexulPrincipal pe deasupra vectorului
                //faceti pe foaie dupa cum scrie aici si vedeti ca iese
                //ideea lui Cucu
                int indexSecundar = indexPrincipal +1;
                int lungimeCurenta = 1;
                double sumaCurenta = array[indexPrincipal];
                
                while(indexSecundar < array.length && array[indexSecundar] > 0 ){
                    //fac suma pentru fiecare subsir pozitive dupa care in urmatorul IF o voi stoca pe cea mai mare
                        sumaCurenta += array[indexSecundar];
                        lungimeCurenta++;
                        indexSecundar++;
                }
                if((lungimeCurenta > lungimeMax) || (lungimeCurenta==lungimeMax && sumaCurenta > sumaMax)){
                        lungimeMax = lungimeCurenta;
                        sumaMax = sumaCurenta;
                        indexMax = indexPrincipal;
                }
                indexPrincipal = indexSecundar + 1;
                        
                    
            
            }else{
                indexPrincipal++;
            }
        }
        
        
        System.out.println(indexMax+" "+lungimeMax);
    }
}
