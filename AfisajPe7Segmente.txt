import java.util.*;

public class AfisajSapteSegmente {
    public static void main(String [] args) {
        Scanner sc = new Scanner(System.in);
        String line = "";
        List<String> list = new ArrayList<>();

        while (sc.hasNextLine()) {
            line = sc.nextLine();
            if(returnNumber(line).equals("NO")) {
                break;
            } else {
                System.out.print(returnNumber(line));
            }
        }
    }

    public static String returnNumber(String line) {
        String [] arr = line.split(",");

        if(arr[0].equals("0") && arr[1].equals("1") && arr[2].equals("1") && arr[7].equals("1") && arr[3].equals("0") &&
                arr[4].equals("0") && arr[5].equals("0") && arr[6].equals("0")) {
            return "1.";
        } else if (arr[0].equals("0") && arr[1].equals("1") && arr[2].equals("1") && arr[3].equals("0") &&
                arr[4].equals("0") && arr[5].equals("0") && arr[6].equals("0") && arr[7].equals("0")) {
            return "1";
        } else if (arr[0].equals("1") && arr[1].equals("1") &&
                arr[2].equals("0") && arr[3].equals("1") && arr[4].equals("1") && arr[5].equals("0")
                && arr[6].equals("1") && arr[7].equals("1")) {
            return "2.";
        } else if(arr[0].equals("1") && arr[1].equals("1") &&
                arr[2].equals("0") && arr[3].equals("1") && arr[4].equals("1") && arr[5].equals("0")
                && arr[6].equals("1") && arr[7].equals("0")) {
            return "2";
        } else if(arr[0].equals("1") && arr[1].equals("1") && arr[6].equals("1") &&
                arr[2].equals("1") && arr[3].equals("1") && arr[7].equals("1") &&
                arr[4].equals("0") && arr[5].equals("0")) {
            return "3.";
        } else if (arr[0].equals("1") && arr[1].equals("1") && arr[6].equals("1") &&
                arr[2].equals("1") && arr[3].equals("1") && arr[4].equals("0") && arr[5].equals("0")
                && arr[7].equals("0")) {
            return "3";
        } else if(arr[5].equals("1") && arr[6].equals("1") && arr[1].equals("1") &&
                arr[2].equals("1") && arr[7].equals("1") && arr[0].equals("0")  && arr[3].equals("0")
                && arr[4].equals("0")) {
            return "4.";
        } else if(arr[5].equals("1") && arr[6].equals("1") && arr[1].equals("1") &&
                arr[2].equals("1") && arr[7].equals("0") && arr[0].equals("0")  && arr[3].equals("0")
                && arr[4].equals("0")) {
            return "4";
        } else if(arr[0].equals("1") && arr[5].equals("1") && arr[6].equals("1") &&
                arr[2].equals("1") && arr[3].equals("1") && arr[7].equals("1") && arr[1].equals("0") &&
                arr[4].equals("0")){
            return "5.";
        } else if (arr[0].equals("1") && arr[5].equals("1") && arr[6].equals("1") &&
                arr[2].equals("1") && arr[3].equals("1") && arr[7].equals("0") && arr[1].equals("0") &&
                arr[4].equals("0")) {
            return "5";
        } else if(arr[0].equals("1") && arr[5].equals("1") && arr[4].equals("1") &&
                arr[6].equals("1") && arr[3].equals("1") && arr[2].equals("1") && arr[7].equals("1")
                && arr[1].equals("0")) {
            return "6.";
        } else if(arr[0].equals("1") && arr[5].equals("1") && arr[4].equals("1") &&
                arr[6].equals("1") && arr[3].equals("1") && arr[2].equals("1") && arr[7].equals("0")
                && arr[1].equals("0")) {
            return "6";
        } else if(arr[0].equals("1") && arr[1].equals("1") && arr[2].equals("1") && arr[7].equals("1") && arr[3].equals("0") &&
                arr[4].equals("0") && arr[5].equals("0") && arr[6].equals("0")) {
            return "7.";
        } else if(arr[0].equals("1") && arr[1].equals("1") && arr[2].equals("1") && arr[3].equals("0") &&
                arr[4].equals("0") && arr[5].equals("0") && arr[6].equals("0") && arr[7].equals("0")) {
            return "7";
        } else if (arr[0].equals("1") && arr[5].equals("1") && arr[4].equals("1") &&
                arr[6].equals("1") && arr[3].equals("1") && arr[2].equals("1") && arr[7].equals("1") && arr[1].equals("1")) {
            return "8.";
        } else if (arr[0].equals("1") && arr[5].equals("1") && arr[4].equals("1") &&
                arr[6].equals("1") && arr[3].equals("1") && arr[2].equals("1") && arr[1].equals("1") && arr[7].equals("0")) {
            return "8";
        } else if(arr[0].equals("1") && arr[5].equals("1") && arr[4].equals("0") &&
                arr[6].equals("1") && arr[3].equals("1") && arr[1].equals("1") && arr[7].equals("1") 
                && arr[2].equals("1")) {
            return "9.";
        } else if (arr[0].equals("1") && arr[5].equals("1") && arr[4].equals("0") &&
                arr[6].equals("1") && arr[3].equals("1") && arr[1].equals("1") && arr[7].equals("0") 
                && arr[2].equals("1")) {
            return "9";
        } else if (arr[0].equals("1") && arr[5].equals("1") && arr[4].equals("1") &&
                arr[3].equals("1") && arr[2].equals("1") && arr[1].equals("1") && arr[7].equals("1") && arr[6].equals("0")) {
            return "0.";
        } else if(arr[0].equals("1") && arr[5].equals("1") && arr[4].equals("1") &&
                arr[3].equals("1") && arr[2].equals("1") && arr[1].equals("1") && arr[6].equals("0") && arr[7].equals("0")) {
            return "0";
        }

        return "NO";
    }
}