import java.util.*;
import java.io.*;

public class Vasi{
    public static void main(String [] args) throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int nrSiruri = Integer.parseInt(br.readLine());
        String line;
        List<String> list = new ArrayList<>();
        List<String> printing = new ArrayList<>();
        
        while((line = br.readLine()) != null) {
            if (line.trim().length() > 0) {
                list.add(line); 
            }
        }
        
        for(int i = 0; i < list.size(); i++) {
            String sir = list.get(i);
            String [] numbers = sir.split(",");
            StringBuilder sb = new StringBuilder();    
            int ip = 0;
            int is = 1;
            int counter = 0;
            
            while(is <= numbers.length) {
                if(is < numbers.length && Integer.parseInt(numbers[is]) == 0) {
                    sb.append("(");
                    sb.append(numbers[ip]);
                    while(is < numbers.length && Integer.parseInt(numbers[is]) == 0) {
                        counter++;
                        is++;
                    }
                    sb.append(",");
                    sb.append(String.valueOf(counter));
                    sb.append(")");
                    ip = is;
                    is++;
                    counter = 0;
                }else {
                    if(!sir.contains("0")) {
                        sb.append(numbers[ip]).append(",");
                    } else if(is == 1) {
                        sb.append(numbers[ip]);
                        sb.append(",");
                    } else if(is == numbers.length) {
                        sb.append(",");
                        sb.append(numbers[ip]);
                    } else {
                        sb.append(numbers[ip]);
                        sb.append(",");
                    }
                    
                    ip = is;
                    is++;
                }
            }
            
            if(!sir.contains("0")) {
                sb.setLength(sb.length() - 1);
            }
            
            printing.add(sb.toString().trim());
        }
        
        for (int i = 0; i < printing.size(); i++) {
            String sir = printing.get(i);
            StringBuffer bf = new StringBuffer(sir);
            int count = 1;
            for (int j = 0; j < sir.length() - 1; j++) {
                char one = sir.charAt(j);
                char two = sir.charAt(j + 1);
                int index = j + count;
                
                if (one == ')' && two == '(') {
                    bf.insert(index, ',');
                    count++;                    
                }
                
                if (index > bf.length()) {
                    j = sir.length() - 1;
                }
            }
            
            if(bf.length() > sir.length()) {
                System.out.println(bf.toString());
            } else {
                System.out.println(sir);    
            }
            
        } 

    }
}