import java.util.*;

public class Vasi{
    public static void main(String [] args){
        Scanner sc = new Scanner(System.in);
        int dimensiuneVector = sc.nextInt();
        int [] array = new int[dimensiuneVector];
        
        for(int i = 0; i < array.length; i++){
            array[i] = sc.nextInt();
        }
        
        int toBeCompared = sc.nextInt();
        Arrays.sort(array);
        int min = 0;
        int max = array.length -1;
        
        while(min <= max){
            int mid = (min + max) / 2;
            /*daca valoarea ce trebuie comparata este egala cu mijlocul vectorului
            printam valoarea cu care s-a facut compararea si iesim din bucla
            Daca nu: daca este mai mare pornim cu index-ul de la valoarea citita +1
            (urmatoarea valoare dupa mijloc)
            Daca nu: daca este mai mic pornim de la 0 pana la valoarea citita -1(valoarea
            dinaintea valorii comparate): aici intervine un caz special: daca dimensiunea vectorului
            cu care ai ramas dupa o comparare este para incluzi si valoarea comparata in vectorul tau
            (la sfarsit e un if in if: pentru asta e)
            */
            if(toBeCompared == array[mid]){
                System.out.println(array[mid]);
                break;
            }else if(toBeCompared > array[mid]){
                min =  mid + 1; 
                System.out.println(array[mid]);
            }else if(toBeCompared < array[mid]){
                max = mid - 1;
                System.out.println(array[mid]);
                if((max - min) % 2 == 1){
                    max = mid;
                }
            }
        }
    }
}